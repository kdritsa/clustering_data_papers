import os
import subprocess
import os
from pdf2image import convert_from_path
import cv2
import pytesseract
from PIL import Image

original_data_path = '../original_data/'
new_data_path = '../_data/'

for filename in os.listdir(original_data_path):
    try:
        command = 'java -jar tika-app-1.20.jar --text ' + original_data_path + filename + '>' + new_data_path + \
                  os.path.splitext(filename)[0] + '.txt'
        subprocess.call(command, shell=True)  # shell=True hides console window
    except:
        print('Could not convert file '+filename)

for filename in os.listdir(new_data_path):

    # if size less than 2KB
    if os.path.getsize(new_data_path+filename)<2000 and not filename.startswith('.'):
        print(filename)

        pdfpath = original_data_path+os.path.splitext(filename)[0]+'.pdf'

        pages = convert_from_path(pdfpath, 500)

        # Break pdf to images
        image_folder = '../outfiles/'+os.path.splitext(filename)[0]+'_images/'
        if not os.path.exists(image_folder):
            os.mkdir(image_folder)
        i=1
        for page in pages:
            page.save(image_folder+'out'+str(i)+'.jpg', 'JPEG')
            i+=1

        # Extract text from images with tesseract
        txt_from_jpgs = []
        #convert jpgs to txt
        for jpg in sorted(os.listdir(image_folder)):
            print(jpg)
            image = cv2.imread(image_folder+jpg)

            # Converting to grayscale
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

            text = pytesseract.image_to_string(Image.open((image_folder+jpg)))
            txt_from_jpgs.append(text)

        # Concatenate text from images and save it as a single pdf
        with open(new_data_path+filename, 'w+', encoding='utf-8') as f:
            f.write("\n".join(txt_from_jpgs))